Ember workshop vm
=================

Vm description
--------------

It is vagrant vm for ember workshop
You need [vagrant](https://www.vagrantup.com/downloads.html) to use it.

Also You need [VirtualBox](https://www.virtualbox.org/wiki/Downloads) and VirtualBox Extension Pack installed.

Also you may or may not need to enable virtualization in bios to use this vm.

Software installed
------------------

More info can be found in `bin/provision.sh`

1. node (0.10.33+ && < 0.11)
2. npm (1.4.28+)
3. bower
4. ember-cli

Next steps
----------

After vm setup with `vagrant up` connect to vm with `vagrant ssh`.

Then navigate to `/var/www/ember-workshop` and execute `ember init`

This way you will setup sample ember app.

After this you can execute `ember serve` to launch it.

App can be accessed here [http://localhost:4200](http://localhost:4200) or [http://192.168.80.10:4200](http://192.168.80.10:4200)

Further exploration
-------------------

1. [https://www.codeschool.com/courses/warming-up-with-ember-js](https://www.codeschool.com/courses/warming-up-with-ember-js)
2. [http://emberjs.com/guides/getting-started](http://emberjs.com/guides/getting-started)